import globals from "globals"
import pluginJs from "@eslint/js"
import pluginHtml from "eslint-plugin-html"


export default [
  {
    languageOptions: {
      globals: globals.browser
    }
  },
  {
    files: ["**/*.html"],
    plugins: { pluginHtml }
  },
  pluginJs.configs.recommended,
];