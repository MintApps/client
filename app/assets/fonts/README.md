# Fonts

## Lete Sans Math

- Font used for math expressions
- Repository [https://github.com/abccsss/LeteSansMath](https://github.com/abccsss/LeteSansMath)
- License [SIL OPEN FONT LICENSE Version 1.1](OFl.txt)
- The OTF font was obtained from [https://github.com/abccsss/LeteSansMath](https://github.com/abccsss/LeteSansMath) and
converted to WOFF2

## Lato Fonts

- Font used for normal text
- Website [https://www.latofonts.com/](https://www.latofonts.com/lato-free-fonts/)
- License [SIL OPEN FONT LICENSE Version 1.1](OFl.txt)
- To reduce loading times, only the Latin characters of this font are loaded; for others, the browser's default "sans" is used.
