# app/assets/icons

This folder contains

- app icons used in Menus (see /app/components/index-menu.js)
- images that are included from Markdown pages (see /app/components/text-block.js)
