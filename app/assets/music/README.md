# Music

Some of the quiz games optionally use background music. 
All songs were composed and recorded by John Bartmann, who makes them available under a CC license on the Free Music Archive (FMA).

## FMA-Links

- https://freemusicarchive.org/music/John_Bartmann/Public_Domain_Soundtrack_Music_Album_One/african-moon
- https://freemusicarchive.org/music/John_Bartmann/80s-action-montage/lets-skate-the-prom/
- https://freemusicarchive.org/music/John_Bartmann/moody-beats/lofi-jam
- https://freemusicarchive.org/music/John_Bartmann/moody-beats/safe-house/
- https://freemusicarchive.org/music/John_Bartmann/Public_Domain_Soundtrack_Music_Album_One/umlungu/

## Permission to use John's music (Email excerpt)

Short answer to your question: yes, go ahead. You may download the files
from FreeMusicArchive and add them to your repo, yes.

However, do please add attribution like this (replacing [TRACK NAME] and
include the title of the selected track in each instance):

*Music [TRACK NAME] by John Bartmann https://youtube.com/johnbartmannmusic
<https://youtube.com/johnbartmannmusic>*

You may also contribute by subscribing to my channel on YouTube
<https://youtube.com/johnbartmannmusic> or making a contribution of any
amount via:

   - Patreon <https://patreon.com/johnbartmann>
   - PayPal <https://paypal.me/johnbartmann>

Music is such an under-valued resource online these days, so I appreciate
it when people give back!

Let me know if you have any questions.
John
~~~