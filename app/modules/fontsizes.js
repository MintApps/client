/**
 * Information about the available font sizes,
 * @author Thomas Kippenberg
 * @module fontsizes
 */

import * as storage from './storage.js'
import { getMintAppsContainer } from './utils.js'

/** current fontsize (small, medium, large)*/
let fontsize
/** List of listener functions (to receive notification on theme changes) */
let listeners = []

/**
 * array of objects, containing available fontsizes;
 * the keys (small, medium, large) correspond to the available options;
 * each entry contains an attribute text (translation key)
 * and an attribute icon (img src for icon)
 */
export const allFontsizes = {
  small: { text: 'Menus.font-small', icon: 'font-small.svg' },
  medium: { text: 'Menus.font-medium', icon: 'font-medium.svg' },
  large: { text: 'Menus.font-large', icon: 'font-large.svg' }
}

/**
 * Initialize the theme module by determining and loading the initial theme (based on user preferences
 */
export function setup() {
  // try to load fontsize preference from storage
  fontsize = storage.get('fontsize')
  if (!fontsize) fontsize = 'medium'
  // get prefered math font from css configuration
  const mathFont = getFontInfo().math
  const preferedMathFont = `${mathFont.size}px ${mathFont.family.split(',')[0]}`
  if (!document.fonts.check(preferedMathFont)) {
    document.fonts.load(preferedMathFont).then(() => {
      console.debug(`fontsizes: preloaded font ${preferedMathFont}`)
      setFontsize(fontsize)
    })
  } else {
    setFontsize(fontsize)
  }
}

/**
 * Set new fontsize
 * @param {String} newFontsize - size of new fontsize (for text)
 * @see allFontsizes
 */
export function setFontsize(newFontsize) {
  fontsize = newFontsize
  storage.set('fontsize', fontsize)
  window.mintapps.container.setAttribute('data-fontsize', fontsize)
  listeners.forEach(listener => listener(fontsize))
}

/**
 * Get current fontsizes
 * @returns {Object} - Font information, for example { text: { familiy: 'Arial', size: 16 }, math: { familiy: 'Times', size: 18 }}
 */
export function getFontInfo() {
  // define fallback values
  let fontFamilyText = 'sans-serif'
  let fontFamilyMath = 'serif'
  let fontSizeText = 15
  let fontSizeMath = 16
  // read css info
  const element = getMintAppsContainer()
  if (element) {
    const style = getComputedStyle(element)
    fontFamilyMath = style.getPropertyValue('--font-family-math')
    fontFamilyText = style.getPropertyValue('--font-family-text')
    fontSizeText = Number(style.getPropertyValue('--font-size-text').replace(/\D/g, ''))
    const styleFsm = style.getPropertyValue('--font-size-math')
    if (styleFsm.endsWith('px')) fontSizeMath = Number(styleFsm.replace(/\D/g, ''))
    else if (styleFsm.endsWith('%')) fontSizeMath = fontSizeText * styleFsm.replace(/\D/g, '') / 100
    else fontSizeMath = fontSizeText
  }
  // answer
  return {
    text: { family: fontFamilyText, size: fontSizeText },
    math: { family: fontFamilyMath, size: fontSizeMath }
  }
}

/**
 * Watch for theme changes
 * @param {Function} listener - listener function (receiving new value)
 */
export function watch(listener) {
  listeners.push(listener)
}
