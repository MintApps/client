/**
 * Helper functions for formatting text
 * @author Thomas Kippenberg
 * @module text
 */
const defaultShortenLength = 100 // default maximum length for shorten function

/**
 * Escape the text so that it can be embedded in HTML without risk
 * @param {string} str - input string
 * @returns {string} escaped string
 */
export function escapeHTML(str) {
  return str.replace(/[&<>"'\\/]/g, char => {
  switch (char) {
    case '&':
      return '&amp;';
    case '<':
      return '&lt;';
    case '>':
      return '&gt;';
    case '"':
      return '&quot;';
    case '\\':
      return '&#39;';
    case '/':
      return '&#x2F;';
    default:
      return char;
    }
  })
}

/**
 * Intelligently shorten a text so that it is not cut off in the middle of a word
 * @param {string} text - input text
 * @param {number} [max=100] - maximum length
 * @returns {string} shortended text
 */
export function shortenText (text, max = defaultShortenLength) {
  const split = /\n|;/
  if (!text || typeof text !== 'string') return ''
  if (text.length < max) return text
  const part = text.split(split)[0]
  if (part.length < max) return part + '…'
  return text.substring(0, max - 1) + '…'
}

/**
 * Convert text to Unicode superscript characters (if possible)
 * @param {String} text 
 */
export function toSuperScript(text) {
  return text
  .replaceAll('-', '⁻')
  .replaceAll('+', '⁺')
  .replaceAll('0', '⁰')
  .replaceAll('1', '¹')
  .replaceAll('2', '²')
  .replaceAll('3', '³')
  .replaceAll('4', '⁴')
  .replaceAll('5', '⁵')
  .replaceAll('6', '⁶')
  .replaceAll('7', '⁷')
  .replaceAll('8', '⁸')
  .replaceAll('9', '⁹')
}
