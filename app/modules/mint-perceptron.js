/**
 * Module that contains the calculations for the mint-percpetron app
 * @author Thomas Kippenberg
 * @module percpetron
 */

/**
 * Calc all node values (forward propagation)
 * @param {number[]} inputs - array of input values
 * @param {number[][][]} weights - array [3d] containing weights
 * @param {number[][]} thresholds - thresholds of the step function
 * @returns output values and values of all nodes
 */
export function calcForward (inputs, weights, thresholds) {
  const outputs = [[]]
  const nets = [[]]
  for (let i = 0; i < inputs.length; i++) outputs[0][i] = inputs[i]
  // loop over source layers
  for (let i = 0; i < weights.length; i++) {
    outputs[i + 1] = []
    nets[i + 1] = []
    // loop over dest nodes
    for (let j = 0; j < weights[i][0].length; j++) {
      // loop over source nodes
      let net = 0
      for (let k = 0; k < weights[i].length; k++) {
        net += weights[i][k][j] * outputs[i][k]
      }
      nets[i + 1][j] = net
      outputs[i + 1][j] = thresholdFunction(net, thresholds[i + 1][j])
    }
  }
  return { outputs, nets }
}

/*
 
 dataSets: array of all datasets
 weights: array [3d] containing weights
 activations: array of activation types
*/
/**
 * Calc mean quadratic error
 * @param {number[]} inputs - array of input values
 * @param {number[][][]} weights - array [3d] containing weights
 * @param {number[][]} thresholds - thresholds of the step function
 * @returns mean square error
 */
export function calcMeanSquareError (dataSets, weights, activations) {
  let E = 0
  for (const dataSet of dataSets) {
    const cf = calcForward(dataSet.inputs, weights, activations)
    E += (dataSet.targets[0] - cf.outputs[weights.length][0]) ** 2
  }
  return E
}

/**
 * Simple threshold function
 * @param {number} x - value
 * @param {number} threshold 
 * @returns 1 if x >= threshold and 0 otherwise
 */
function thresholdFunction (x, threshold) {
  return x >= threshold ? 1 : 0
}
