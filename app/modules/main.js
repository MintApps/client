/**
 * Main module
 * Setup mintapp framework, only start once per page load
 * @author Thomas Kippenberg
 * @module main
 * @see {@link https://codeberg.org/MintApps/weblate|Mintapps weblate repository}
 */

import * as i18n from './i18n.js'
import { sitemap } from '../sitemap.js'
import * as themes from './themes.js'
import * as fontsizes from './fontsizes.js'
import * as sync from './sync.js'
import { initMath, supportsMath } from './asciimathml.js'
import config from '../config.js'
import { getMintAppsId, getMintAppsContainer } from './utils.js'

// prevent flouc
const mintappContainer = getMintAppsContainer()
if (sessionStorage.getItem('theme') === 'dark') document.body.style.backgroundColor = '#000000'

// check if this is a website
window.mintapps = {}
window.mintapps.website = config.website

// check configured path names and append trailing slash if necessary
for (const i of ['syncUrl', 'downloadUrl', 'dataPath']) {
  if (typeof config[i] === 'string' && !config[i].endsWith('/'))
    console.error(`Configuration: key config.${i} must end with trailing slash`)
}

// load app meta data
let app
const id = getMintAppsId(mintappContainer)
if (id === 'index') app = { title: sitemap.title, text: sitemap.text }
else {
  sitemap.topics.forEach(topic => {
    if (topic.id === id) app = topic
    if (!app && topic.subtopics) {
      topic.subtopics.forEach(subtopic => {
        if (subtopic.id === id) app = subtopic
        if (!app && subtopic.sites) {
          subtopic.sites.forEach(site => {
            if (site.id === id) app = site
          })
        }
      })
    }
  })
}
if (!app) {
  console.error(`Configuration: Parameter data-id missing or wrong in main mintapp element`)
}

// store basic infos on app
if (!window.mintapps) window.mintapps = {}
window.mintapps.container = mintappContainer
Object.keys(app).forEach(key => window.mintapps[key] = app[key])

// load top / bottom navigation and app-info
if (window.mintapps?.website) {
  import('../components/navbar-top.js').then(() => { mintappContainer.prepend(document.createElement('navbar-top')) })
  import('../components/app-info.js').then(() => {
    import('../components/navbar-bottom.js').then(() => {
      mintappContainer.append(document.createElement('app-info'))
      mintappContainer.append(document.createElement('navbar-bottom'))
    })
  })
}
// initialize i18n
i18n.setup()

// init asciimath
initMath({})
if (!supportsMath()) {
  const div = document.createElement('div')
  div.classList.add('alert', 'alert-warning')
  div.style.flexGrow = '0'
  div.setAttribute('data-i18n-md-inline', 'NoMathML.info')
  mintappContainer.prepend(div)
}

// init theme and start listener to theme changes
themes.setup()

// set fontsize
fontsizes.setup()

// confirm unload when syncing or authenticated
window.addEventListener('beforeunload', (event) => {
  if (window.mintapps.modified || sync.isSyncing()) {
    event.preventDefault()
  }
})

// wait until document is loaded
window.onload = () => {
  // show page
  if (window.mintapps.website) document.body.style.visibility = 'visible'
  // translate all elements after loading the document and fonts
  i18n.translateChildren(mintappContainer)
}

