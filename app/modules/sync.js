/**
 * Module offering methods for syncing to other app instances, using the MintApps sync-api
 * @author Thomas Kippenberg
 * @module sync
 * @see https://codeberg.org/MintApps/server
*/

import * as http from '../modules/http.js'
import config from '../config.js'

// sync params
const NODE_RETRY_INTERVAL = 5000 // delay between retries, when connection is lost
const NODE_RETRY_MAX_NUM = 10 // maximum number of retries, when connection is lost

// Nick names for guests
const nickNames = ['Ampere', 'Aristoteles', 'Archimedes', 'Bernoulli', 'Bohr', 'Carnot', 'Cavendish', 'Coulomb', 'Curie', 'Einstein', 'Euler', 'Faraday', 'Feynman', 'Fibonacci', 'Fourier', 'Fresnel', 'Gilbert', 'Galilei', 'Gauß', 'Hamilton', 'Hawking', 'Heisenberg', 'Helmholtz', 'Hertz', 'Huygens', 'Kepler', 'Kirchhoff', 'Kelvin', 'Kopernikus', 'Laplace', 'Leibniz', 'Lorentz', 'Joule', 'Maxwell', 'Newton', 'Pascal', 'Pauli', 'Planck', 'Pythagoras', 'Rutherford', 'Röntgen', 'Schrödinger', 'Thomson', 'Turing', 'Volta', 'Young']
const nameCache = {} // cache to assign same name, when user reenters room

// common attributes
let syncing = false // module is in sync mode
let connected = false // user connected (eventually disconnected for a short time while syncing)
let stopped = false // sync has been stopped by user (to avoid reconnect attempts)
let syncPos = 0
let numRetries = 0 // counter retries on connection errors
let listener = () => { } // listen to messages of room members
let ownerListener = null // specific listener for room owner, which receives messages about member changes
let room = null // current room id
let user = null // own user id
let token = null // own token
let owner = null // room owner
let members = [] // list of all members in room, only available for owner
let nameIndex = 0 // index for assigning names
let webSocket = null // websocket - only for node server

// mix nickNames
for (let i = nickNames.length - 1; i > 0; i--) {
  const j = Math.floor(Math.random() * (i + 1));
  [nickNames[i], nickNames[j]] = [nickNames[j], nickNames[i]]
}

/**
 * Set listener for room messages
 * @param {function} _listener - listener function for room messages
 */
export function setRoomListener(_listener) {
  listener = _listener
}

/**
 * set listener for specific events to room owner
 * @param {function} _ownerListener  - listener function for room-owner messages
 */
export function setOwnerListener(_ownerListener) {
  ownerListener = _ownerListener
}

/**
 * Create a new room for exchanging sync messages
 * @param {string} name - name of room
 * @param {string} type - type of room (for example 'pairs', 'quiz', etc.)
 * @param {string} [_user] - optional user name of authentificated user
 * @param {string} [_secret] - optional session token of authentificated user
 * @returns {Promise} with {string} room id
 */
export function createRoom(name, type, _user, _secret) {
  return new Promise((resolve, reject) => {
    if (!check()) {
      reject(new Error('no-sync-server'))
      return
    }
    http
      .post('sync:create-room', { name, type, user: _user, secret: _secret })
      .then((response) => {
        room = response.room
        user = response.user
        token = response.token
        owner = response.owner
        // open websocket connection for node backend
        doSyncNode().then(resolve).catch(reject)
      })
      .catch(reject)
  })
}

/**
 * Join existing room (or reenter when user and roken given)
 * @param {string} _room - id of room ro (re)enter
 * @param {string} [_user] - optional user id for reentering
 * @param {string} [_token] - optional room token for reentering
 * @returns {Promise}
 */
export function joinRoom(_room, _user = null, _token = null) {
  return new Promise((resolve, reject) => {
    if (!check()) {
      reject(new Error('no-sync-server'))
      return
    }
    user = _user
    token = _token
    syncPos = 0
    room = _room
    doSyncNode().then(resolve).catch(reject)
  })
}

/**
 * Get id of current room
 * @returns {string} - id of current room
 */
export function getRoom() {
  return room
}

/**
 * Set or change name of current room (only as owner)
 * @param {string} name - new room name
 * @returns {Promise}
 */
export function setRoomName(name) {
  const params = { room, user, token, name }
  return new Promise((resolve, reject) => {
    http.post('sync:set-room-name', params).then(resolve).catch(reject)
  })
}

/**
 * Delete room (only as owner)
 * @param {string} room - room id
 * @param {string} user - user id
 * @param {string} token - room token
 * @returns {Promise}
 */
export function deleteRoom(room, user, token) {
  return new Promise((resolve, reject) => {
    members = []
    http.post('sync:delete-room', { room, user, token }).then(resolve).catch(reject)
  })
}

/**
 * Send message to all members or an single member (of current room)
 * @param {string} dst - id of destination (* for all members of room)
 * @param {object} content - message
 */
export function sendMessage(dst, content) {
  if (!syncing) throw new Error('room-expired')
  if (!connected) throw new Error('server-no-connection')
  else {
    webSocket.send(JSON.stringify({ action: 'send-room-message', to: dst, content }))
  }
}

/**
 * Check, if user is owner of current room
 * @returns  {boolean} true if owner
 */
function isOwner() {
  return owner && owner === user
}

/**
 * Get owner of current room
 * @returns {string} - id of current owner
 */
export function getOwner() {
  return owner
}

/**
 * Check if current room is syncing (e.g. is connected to server)
 * @returns {boolean} true if currently connected
 */
export function isSyncing() {
  return syncing
}

/**
 * Stop syncing and delete room when user = owner
 */
export async function stop() {
  // do nothing when not syncing
  if (!syncing) return
  // prevent from automatic reconnect
  stopped = true
  // delete room when owner
  if (isOwner()) await deleteRoom(room, user, token)
  // else send info, that user is leaving room
  else webSocket.send(JSON.stringify({ action: 'leave-room' }))
  // wait until all messages are sent
  while (webSocket.bufferedAmount > 0) {
    await delay(100)
  }
  // close websocket connection
  webSocket.close()
  // reset values
  webSocket = null
  room = null
  syncing = false
  connected = false
}

/**
 * Delay function
 * @param {Number} ms - delay in milliseconds
 * @returns {Promise} promise that resolves after specified delay
 */
export function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}


/**
 * Get room members (only for owner)
 * @returns {object[]} list, containing objects {name, id}
 */
export function getMembers() {
  return members
}

/**
 * Get number of members (only for owner)
 * @returns {number} number of members currently in the room
 */
export function getCountMembers() {
  return members.length
}

/* Internal functions */

/**
 * check configuration
 * @returns true if it is possible to create new sync rooms
 */
function check() {
  return document.URL.startsWith('http') && config.syncUrl
}

/**
 * Sync function for node server, uses websocket
 * @returns {Promise}
 */
function doSyncNode() {
  return new Promise((resolve, reject) => {
    const url = 'ws' + config.syncUrl.substring(4) + 'socket'
    webSocket = new WebSocket(url)
    debug('Open websocket: ' + config.syncUrl)
    webSocket.onopen = () => {
      debug('Websocket: connected')
      connected = true
      webSocket.send(JSON.stringify({ action: 'join-room', user, room, token, pos: syncPos }))
      numRetries = 0
    }
    webSocket.onerror = () => {
      connected = false
      if (numRetries === 0) reject(new Error('server-no-connection'))
      debug('Websocket: error')
    }
    webSocket.onclose = () => {
      connected = false
      debug('Websocket: closed')
      if (!stopped) setTimeout(() => reconnectWS(), NODE_RETRY_INTERVAL)
    }
    webSocket.onmessage = (msg) => {
      let data = {}
      try {
        data = JSON.parse(msg.data)
      } catch (e) {
        debug(e)
      }
      if (data.error === 'room-expired') {
        if (!stopped) {
          syncing = false
          webSocket.close()
          reject(new Error('room-expired'))
        }
      } else if (data.error) {
        listener('', { error: data.error })
      } else if (data.action === 'set-room') {
        // joined room?
        room = data.room
        syncing = true
        stopped = false
        user = data.user
        token = data.token
        owner = data.owner
        resolve(room)
      } else if (data.action === 'add-member') {
        addRoomMember(data.member)
      } else if (data.action === 'delete-member') {
        deleteRoomMember(data.member)
      } else if (data.action === 'room-message' && listener) {
        syncPos = Math.max(syncPos, data.pos)
        if (data.from && data.content) listener(data.from, data.content)
      } else if (data.action === 'room-messages' && listener) {
        for (let i = 0; i < data.messages.length; i++) {
          const message = data.messages[i]
          syncPos = Math.max(syncPos, message.pos)
          listener(message.from, message.content)
        }
      }
    }
  })
}

/**
 * Check Websocket connection and reconnect if necessary
 */
function reconnectWS() {
  if (!syncing) return
  numRetries++
  if (numRetries > NODE_RETRY_MAX_NUM) {
    listener('', { error: 'server-connection-closed' })
    webSocket = null
    room = null
    syncing = false
    connected = false
  } else {
    debug('Websocket: trying to reconnect')
    doSyncNode()
      .then(() => {
        debug('Websocket: successfully reconnected')
      })
      .catch((err) => {
        syncing = false
        debug(err, 'Websocket: failed to reconnect')
        numRetries++
        setTimeout(() => reconnectWS(), NODE_RETRY_INTERVAL)
      })
  }
}

/**
 * Add member to room and create nickname
 * @param {string} memberId - id of new member
 */
function addRoomMember(memberId) {
  if (owner === memberId) return // owner is noa guest
  let name = nameCache[memberId] // already name for this member assigned (on reenter)?
  if (!name) {
    const partNum =
      nameIndex >= nickNames.length
        ? ' ' + Math.floor(nameIndex / nickNames.length)
        : ''
    const partName = nickNames[nameIndex % nickNames.length]
    name = partName + partNum
    nameCache[memberId] = name
    nameIndex++
  }
  members.push({ name, id: memberId })
  try {
    sendMessage(memberId, { action: 'welcome', name })
  } catch (e) {
    debug(e)
  }
  if (ownerListener) ownerListener({ members })
}

/**
 * Delete member of room (locally, no propagation to other members)
 * @param {string} memberId - id of deleted member
 */
function deleteRoomMember(memberId) {
  members = members.filter((m) => {
    return m.id !== memberId
  })
  if (ownerListener) ownerListener({ members })
}

/**
 * Internal debug function
 * @param {Error} e 
 */
function debug(e) {
  console.debug(e)
}
