/**
 * Module that allows easy access via get and post, using XMLHttpRequest
 * @author Thomas Kippenberg
 * @module http
 * @see https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
 */
import config from '../config.js'

/**
 * Send POST request
 * @param {string} to - request endpoint; if a relative path is specified, it automatically refers to the SyncServer
 * @param {object} params - parameter object with key value pairs, for example { name: max, age: 17 }
 * @returns {object} response json object (from XMLHttpRequest)
 */
export function post (to, params) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    const url = buildUrl(to)
    xhr.open('POST', url, true)
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
    xhr.responseType = 'json'
    if (to.startsWith('sync:')) xhr.withCredentials = true
    xhr.onload = () => {
      if (xhr.status !== 200) reject(new Error('server-no-connection'))
      else if (!xhr.response) reject(new Error('server-no-connection'))
      else if (xhr.response.error) reject(new Error(xhr.response.error))
      else resolve(xhr.response)
    }
    xhr.send(buildParamString(params))
    xhr.onerror = () => {
      reject(new Error('server-no-connection'))
    }
  })
}

/**
 * Send GET request
 * @param {string} to - request endpoint, see {@link module:http.buildUrl}
 * @param {object} params - parameter object with key value pairs, for example { name: max, age: 17 }
 * @returns {object} response json object (from XMLHttpRequest)
 */
export function get (to, params = {}) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    const url = buildUrl(to) + buildParamString(params)
    xhr.open('GET', url, true)
    xhr.responseType = 'json'
    xhr.onload = () => {
      if (xhr.status !== 200) reject(new Error('server-no-connection'))
      else if (!xhr.response) reject(new Error('server-no-connection'))
      else if (xhr.response.error) reject(new Error(xhr.response.error))
      else resolve(xhr.response)
    }
    xhr.send()
    xhr.onerror = () => {
      reject(new Error('server-no-connection'))
    }
  })
}


/**
 * create encoded parameter string
 * @param {string} to - request endpoint, see {@link module:http.buildUrl}
 * @returns {string} encoded string, for example name=max&age=17
 */
function buildParamString(params) {
  if (!params) return ''
  let result = ''
  let isFirst = true
  for (const [key, value] of Object.entries(params)) {
    if (!isFirst) result += '&'
    result += key + '=' + encodeURIComponent(value)
    isFirst = false
  }
  return result
}

/**
 * Build URL from to parameter
 * @param {string} to - if to starts with 'data:', the URL is prefixed with the variable config.dataPath; if it starts with 'sync:', the variable 'config.syncUrl' is prefixed.
 * @returns {string} full url
 */
function buildUrl(to) {
  if (to.startsWith('data:')) return config.dataPath + to.substring(5)
  else if (to.startsWith('sync:')) return config.syncUrl + to.substring(5)
  else return to
}