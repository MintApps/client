/**
 * Module for calculating the Julia or Mandelbrot set line by line
 * @author Thomas Kippenberg
 * @module julia
 */

let scaleX, scaleY, cRe, cIm, aRe, aIm, isJulia, params, done, y

/**
 * Initialization of the class, finding attractors for the Julia set
 * @param {object} p - calculation params
 * @param {string} p.set - type of set ('julia' or 'mandelbrot')
 * @param {number} p.cRe - left edge of the section from the Gaussian number plane (real part)
 * @param {number} p.cIm - upper edge of the section from the Gaussian number plane (imaginary part)
 * @param {number} p.width - width of the section of the Gaussian number plane (real part)
 * @param {number} p.height - height of the section of the Gaussian number plane (imaginary part)
 * @param {number} p.pixelWidth - width of image in px
 * @param {number} p.pixelHeight - height of image in px
 */
export function init(p) {
  params = p
  // calculate scale
  scaleX = params.width / params.pixelWidth
  scaleY = params.height / params.pixelHeight
  cRe = params.cRe
  cIm = params.cIm
  aRe = 0
  aIm = 0 // attractor
  isJulia = params.set === 'julia'
  // search attractor for Julia set, which must not be root of the equation
  if (isJulia) {
    let zRe = 0
    let zIm = 0
    let zRe2 = 0
    let zIm2 = 0
    for (let i = 0; i < 10000 && !isNaN(zRe) && !isNaN(zIm); i++) {
      zIm = 2 * zRe * zIm + cIm
      zRe = zRe2 - zIm2 + cRe
      zRe2 = zRe * zRe
      zIm2 = zIm * zIm
    }
    // attractor found ?
    if (!isNaN(zRe) && !isNaN(zIm)) {
      aRe = zRe
      aIm = zIm
    }
    params.depth = 4 * params.depth
    // set current line
  }
  y = 0
  done = false
}

/**
 * Calculate the next line of the fractal
 * @param {function} callback - Function that is called when the calculation of the series has been completed;
 * The function has two parameters, the y-value (pixel) and an number[] array with the calculated recursion depths
 */
export function calcNextLine(callback) {
  if (done) {
    callback(y, null)
    return
  }
  const row = []
  for (let x = 0; x < params.pixelWidth; x++) {
    let zIm = 0
    let zRe = 0
    let cRe = params.cRe
    let cIm = params.cIm
    if (!isJulia) {
      // Mandelbrot
      cIm = params.cornerIm + y * scaleY
      cRe = params.cornerRe + x * scaleX
    } else {
      // Julia
      zIm = params.cornerIm + y * scaleY
      zRe = params.cornerRe + x * scaleX
    }
    let step = 0
    let zRe2 = zRe * zRe
    let zIm2 = zIm * zIm
    let da = Infinity // distance to attractor

    // Start recursion
    do {
      zIm = 2 * zRe * zIm + cIm
      zRe = zRe2 - zIm2 + cRe
      zRe2 = zRe * zRe
      zIm2 = zIm * zIm
      step = step + 1
      // calc distance to attractor
      if (isJulia) {
        da =
          (zRe - aRe) * (zRe - aRe) +
          (zIm - aIm) * (zIm - aIm)
      }
    } while (step < params.depth && zRe2 + zIm2 < 4 && da > 1e-6)
    // Store result
    row[x] = da > 1e-6 ? step : -step
  }
  callback(y, row)
  y = y + 10
  if (y > params.pixelHeight) {
    y = (y % 10) + 1
    done = y === 10
  }
}

/**
 * Stop the calculation of the current line
 */
export function stop() {
  done = true
}

