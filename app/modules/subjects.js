/**
 * Information about the available subjects,
 * @author Thomas Kippenberg
 * @module subjects
 */

/**
 * array of objects, containing available subjects;
 * the keys (maths, physics, etc.) correspond to the available options;
 * each entry contains an attribute text (translation key)
 */
export const allSubjects = {
  chemistry: { text: 'Menus.chemistry'},
  history: { text: 'Menus.history' },
  games: { text: 'Menus.games' },
  informatics: { text: 'Menus.informatics'},
  mathematics: { text: 'Menus.mathematics' },
  music: { text: 'Menus.music' },
  physics: { text: 'Menus.physics' }
}
