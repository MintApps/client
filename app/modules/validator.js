/**
 * Module with functions for escaping and shortening of input values
 * @module validator
 * @author Thomas Kippenberg
 */

// Regular expressions for input validation
const REGEX_TEXT = /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}]*$/gu
const REGEX_TEXT_AREA = /^[\p{L}\p{M}\p{N}\p{P}\p{S}\p{Z}\n\t\r]*$/gu
const REGEX_DATA_URL = /^(data:)([\w/+-]*)(;charset=[\w-]+|;base64){0,1},(.*)/gi
const REGEX_COLOR = /^#[0-9a-fA-F]{6}$/
const MAX_SIZE_DATA_URL = 1E6

/**
 * Validate string
 * @param {string} input - input string
 * @param {boolean} required - true if the string cannot be empty
 * @param {number} maxLength - maximum lenght of input (0 = unlimited)
 * @param {RegExp} pattern - regular expression
 * @returns {boolean}
 */
function validateString(input, required, maxLength, pattern) {
  try {
    if (!pattern) return false
    if (typeof input !== 'string') return false
    if (input === '' && !required) return true
    if (input === '') return false
    if (maxLength > 0 && input.length > maxLength) return false
    return input.toString().match(pattern)
  } catch {
    return false
  }
}

/**
 * Validate text input
 * @param {string} input - input string
 * @param {boolean} required - true if the string cannot be empty
 * @param {number} maxLength - maximum lenght of input (0 = unlimited)
 * @returns {boolean}
 */
export function validateText(input, required, maxLength = 0) {
  return validateString(input, required, maxLength, REGEX_TEXT)
}

/**
 * Validate textarea input, where line breaks are allowed
 * @param {string} input - input string
 * @param {boolean} required - true if the string cannot be empty
 * @param {number} maxLength - maximum lenght of input (0 = unlimited)
 * @returns {boolean}
 */
export function validateTextArea(input, required, maxLength = 0) {
  return validateString(input, required, maxLength, REGEX_TEXT_AREA)
}

/**
 * Validate color input
 * @param {string} input - color in hexadecimal format, for example #ff4530
 * @param {boolean} required - true if the color cannot be empty
 * @returns {boolean}
 */
export function validateColor(input, required = true) {
  return validateString(input, required, 0, REGEX_COLOR)
}

/**
 * Validate dataUrl, only superficial examination of the beginning, not of the content itself
 * @param {string} input - dataUrl
 * @param {boolean} required - true if the input cannot be empty
 * @param {number} maxLength - maximum lenght of inpu (0 = unlimited)
 * @returns {boolean}
 */
export function validateDataUrl(input, required = true, maxLength = MAX_SIZE_DATA_URL) {
  return validateString(input, required, maxLength, REGEX_DATA_URL)
}

/**
 * Validate integer number
 * @param {number} input - input number
 * @param {number} min - minimum value
 * @param {number} max - maximum value
 * @returns {boolean}
 */
export function validateInteger(input, min, max) {
  if (!Number.isInteger(input)) return false
  return input >= min && input <= max
}

/**
 * Validate float number
 * @param {number} input - input number
 * @param {number} min - minimum value
 * @param {number} max - maximum value
 * @returns {boolean}
 */
export function validateNumber(input, min = -1E100, max = 1E100) {
  if (typeof input !== 'number') return false
  return input >= min && input <= max
}

/**
 * Test, on of root's children has an validation error (identified by the class .invalid)
 * @param {HTMLElement} root 
 * @returns {Boolean}
 */
export function hasValidationErrors(root) {
  if (!root) root = document.body
  const validationErrors = root.querySelectorAll('.invalid').length
  return validationErrors > 0
}