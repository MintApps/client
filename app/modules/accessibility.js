/**
 * Accessibility module offering methodes for improved accessibility
 * @author Thomas Kippenberg
 * @module accessability
 */

import { translate } from './i18n.js'

const synth = window.speechSynthesis
let readerState = 0

/**
 * Add keylistener to all child elemends with class "enter-click", so that enter will trigger an click event 
 * @param {HTMLElement} parent - parent element
 */
export function enableEnterToClick(parent) {
  const elements = parent.querySelectorAll('.enter-click')
  elements.forEach((element) => {
    element.tabIndex = 0
    element.addEventListener('keypress', eventHandlerEnterToClick)
  })
}

/**
 * Trigger click event, if enter key is pressed
 * @param {Event} e - keypress event to analyse
 */
function eventHandlerEnterToClick(e) {
  if (e.keyCode === 13) e.target.click()
}

/**
 * Start screen reader
 * @param {String} text - text to read (plaintext)
 */
export function readText(text) {
  startReader(text)
}

/**
 * Start screen reeder
 * @param {String} md  - markdown text to read
 */
export function readMarkdown(md) {
  startReader(markdown2Text(md))
}

/**
 * Start screen reeder
 * @param {HTMLElement} node - html node to read
 */
export function readNode(node) {
  startReader(node2Text(node))
}

/**
 * Start screen reader
 * @param {String} text - plain text to speak
 */
function startReader(text) {
  if (readerState !== 0) synth.cancel()
  readerState = 1
  const utterance = new SpeechSynthesisUtterance(text)
  synth.speak(utterance)
}

/**
 *  Stop reader (sytem wide, as there is just one reader per website)
 */
export function stopReader() {
  synth.cancel()
  readerState = 0
}

/*
  Checks, if this instance is currently reading
*/
export function isReading() {
  return readerState !== 0
}

/**
 * Convert markdown text to plain text, currently not used in MintApps
 * @param {String} markdownText 
 * @returns {String} plain text
 */
function markdown2Text(markdownText) {
  const plainText = markdownText
    .replace(/^### (.*$)/gim, '$1')
    .replace(/^## (.*$)/gim, '$1>')
    .replace(/^# (.*$)/gim, '$1')
    .replace(/^> (.*$)/gim, '$1')
    .replace(/\*\*(.*)\*\*/gim, '$1')
    .replace(/\*(.*)\*/gim, '$1')
    .replace(/!\[(.*?)\]\((.*?)\)/gim, "<img alt='$1' src='$2' />")
    .replace(/\[(.*?)\]\((.*?)\)/gim, "<a href='$2'>$1</a>")
  return plainText.trim()
}

/**
 * Parse HTML Node recursive and generate Text to speek
 * @param {HTMLElement} node 
 * @returns {String} plain text
 */
function node2Text(node) {
  const tagName = node?.tagName?.toLowerCase()
  const nodeName = node?.nodeName
  const classList = node?.classList
  const style = tagName ? window.getComputedStyle(node) : {}
  // check for recursion end
  if (nodeName === '#text' && node.nodeValue) return node.nodeValue + ' '
  if (
    /^custom-slider|mint-card-icon|entry-image|nav-top|nav-bottom|menu-mask|katex-mathml$/.test(
      classList
    )
  ) { return '' }
  if (style?.visibility === 'hidden') return ''
  if (style?.display === 'none') return ''
  if (tagName === 'input') {
    if (node.type !== 'checkbox') return node.value + ' '
    return ''
  }
  if (tagName === 'textarea') return node.value + ' '
  if (tagName === 'img' && node.alt) return node.alt + ' '
  if (tagName === 'img' && node.ariaLabel) return node.ariaLabel + ' '
  if (tagName === 'canvas') { return translate('ScreenReader.canvas', node) + ' ' }
  if (tagName === 'select') {
    if (node.selectedIndex >= 0) { return node.options[node.selectedIndex].text + ' ' }
    return ''
  }
  let text = ''
  // insert additional text
  if (classList?.contains('mint-card-header')) { text += translate('ScreenReader.card') + ' ' }
  if (tagName === 'button') text += translate('ScreenReader.button') + ' '
  // recursion
  for (const child of node.childNodes) {
    text += node2Text(child)
  }
  if (/^p|div|tr|h1|h2|h3|button$/.test(tagName) && text) {
    text = text.trim() + '\n'
  }
  return text
}
