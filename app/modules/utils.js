/**
 * Module with several utility functions for manipulating and accessing html elements
 * @author Thomas Kippenberg
 * @module utils
 */

/** Counter for unique id */
let uidCounter = 1
/** List of already imported css files, to avoid duplicates */
const importedCssFiles = []
/** List of already loaded scripts, to avoid duplicates */
const loadedScriptFiles = []

/**
 * Show or hide the element depending on a condition
 * @param {HTMLElement} element - HTML element
 * @param {Boolean} show - condition
 */
export function showIf(element, show = true) {
  if (!element) throw new Error('missing element / identifier')
  if (typeof element === 'string') element = document.getElementById(element)
  if (element) {
    if (show) element.removeAttribute('data-hide')
    else element.setAttribute('data-hide', '')
  }
}

/**
 * Hide one or more elements
 * @param  {...HTMLElement} elements
 */
export function hide(...elements) {
  elements.forEach(element => {
    if (typeof element === 'string') element = document.getElementById(element)
    if (element) element.setAttribute('data-hide', '')
  })
}

/**
 * Show one or more elements
 * @param  {...any} elements
 */
export function show(...elements) {
  elements.forEach(element => {
    if (typeof element === 'string') element = document.getElementById(element)
    if (element) element.removeAttribute('data-hide', '')
  })
}

/**
 * Show or hide elements with attribute 'data-details' depending on a condition
 * @param {HTMLElement} root - parent node
 * @param {Boolean} condition - condition
 */
export function showIfDetails(root = document.body, condition = true) {
  root.querySelectorAll("[data-detail]").forEach(element => showIf(element, condition))
}

/**
 * Shorthand for doucment.getElementById
 * @param {String} id - id of element
 * @param {String} [value] - initial value of element
 * @returns {HTMLElement}
 */
export function gid(id, value = null) {
  const element = document.getElementById(id)
  if (element && value !== null) element.value = value
  return element
}

/**
 * Dispatch event with params
 * @param {EventTarget} element - event target element
 * @param {String} name - name of event
 * @param {Objects} [detail={}] optional parameter object
 */
export function emit(element, name, detail = {}) {
  const event = new CustomEvent(name, { detail })
  element.dispatchEvent(event)
}

/**
 * Prevent default action of event
 */
export function prevent(e) {
  e.preventDefault()
}

/**
 * Create unique (but not random!) id
 * @returns {String}
 */
export function uid() {
  return `mintapp-id-${uidCounter++}`
}

/** 
 * Get specific url param
 * @param {String} name - name of parameter
 * @returns {String} content, or null if not present
 */
export function getUrlParam(name) {
  const queryString = window.location.search;
  const urlSearchParams = new URLSearchParams(queryString);
  return urlSearchParams.get(name)
}

/** 
 * Set specific url param without reloading the page
 * @param {String} name - name of parameter
 * @param {String} value - value of param
 */
export function setUrlParam(name, value) {
  const url = new URL(window.location.href)
  url.searchParams.set(name, value)
  history.replaceState({}, "", url);
}

/** 
 * Delete specific url param without reloading the page
 * @param {String} name - name of parameter
 */
export function deleteUrlParam(name) {
  const url = new URL(window.location.href)
  url.searchParams.delete(name)
  history.replaceState({}, "", url);
}

/**
 * Create href value for given app-id and set of params
 * @param {String} appId - id of app
 * @param {Object} params - parameter object (key → value)
 * @returns {String} href
 */
export function buildHref(appId, params = {}) {
  // convert legacy links of MintApps version 1.x (for example mint-pairs-play-autumn → mint-pairs?play=autumn)
  const playIndex = appId.indexOf('-play-')
  if (playIndex > 0) {
    params.play = appId.substring(playIndex + 6)
    appId = appId.substring(0, playIndex)
  }
  const urlParams = new URLSearchParams()
  // urlParams.set('lang', getCurrentLocale())
  Object.keys(params).forEach(key => {
    urlParams.set(key, params[key])
  })
  if (urlParams.toString()) return `${appId}.html?${urlParams.toString()}`
  else return `${appId}.html`
}

/**
 * Get container of element current mintapp (e.g. element with class 'mintapps')
 * @returns {HTMLElement} container element
 * @see module:sitemap
 */
export function getMintAppsContainer() {
  const container = document.querySelector('.mintapps')
  if (!container) {
    console.error('utils: central mintapps-container with class "mintapps" is missing')
    return document.body
  } else {
    return container
  }
}

/**
 * Get id of current mintapp by reading the attribute 'data-id' from the app-container (with class 'mintapps')
 * @param {HTMLElement} container - optional app-container element
 * @returns {String} unique id of current mintapp
 * @see module:sitemap
 */
export function getMintAppsId(container) {
  if (!container) container = getMintAppsContainer()
  const id = container.getAttribute('data-id')
  if (!id) console.error('utils: data-id attribute is missing in mintapps-container')
  return id
}


/** 
 * Dynamic loading of a stylesheet that is included in the header. Interim solution until CSS modules are supported in all browsers.
 * @param {String} file - filename of css relativ to style dir
 */
export function importCss(cssFile) {
  if (importedCssFiles.indexOf(cssFile) < 0) {
    importedCssFiles.push(cssFile)
    const link = document.createElement('link')
    link.rel = 'stylesheet'
    link.href = `../assets/styles/${cssFile}`
    document.head.append(link)
  }
}

/**
 * Load Javascript file dynamically
 * @param {String} scriptFile - file name of script
 */
export function loadScript(scriptFile) {
  return new Promise((resolve, reject) => {
    if (loadedScriptFiles.indexOf(scriptFile) < 0) {
      loadedScriptFiles.push(scriptFile)
      const scriptTag = document.createElement('script')
      scriptTag.onload = resolve
      scriptTag.onerror = reject
      scriptTag.src = scriptFile
      document.head.appendChild(scriptTag)
    } else {
      resolve()
    }
  })
}

/**
 * Simple reactive value
 */
export class ref {
  currentValue = null
  listeners = []

  constructor(initialValue) {
    this.currentValue = initialValue
  }

  /**
   * Set new value and inform listeners
   * @param {Object} newValue 
   */
  set value(newValue) {
    if (newValue !== this.currentValue) {
      const oldValue = this.currentValue
      this.currentValue = newValue
      for (const listener of this.listeners) {
        listener(oldValue, newValue)
      }
    }
  }

  /**
   * Get current value
   * @returns {Object}
   */
  get value() {
    return this.currentValue
  }

  /**
   * Add listener function with two parameters (oldValue, newValue)
   * @param {Function} listener 
   */
  watch(listener) {
    this.listeners.push(listener)
  }
}