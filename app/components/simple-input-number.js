import { translateChildren, useDecimalSeparatorComma } from '../modules/i18n.js'
import { HTMLMintElement } from './abstract-components.js'
import { watch as watchLocale } from '../modules/i18n.js'
const regexNumber = /^[+-]?[0-9]+(?:[.,][0-9]+)?(?:[eE][+-]?[0-9]+)?$/ // regex for validating number inputs
const regexInteger = /^[+-]?[0-9]+$/ // regex for validating integer inputs

// Watch the current language to determine whether a comma or a period should be used as a decimal separator
let comma = true
watchLocale(() => {
  comma = useDecimalSeparatorComma()
})

/**
 * Simple number input field without buttons or slider to used outside "normal" forms
 * @property {Boolean} [data-integer] require integer numbers (not reactive)
 * @property {Number} [data-min=0]
 * @property {Number} [data-max=1]
 */
class SimpleInputNumber extends HTMLMintElement {
  static observedAttributes = ['data-min', 'data-max', 'data-integer', 'disabled']
  #min = 0 // Minimum value
  #max = 10 // Maximum value
  #integer = false // Accept only interger values
  #value = 0 // Current value of input field
  #input = null // Reference to input element
  #info = null // Reference to optional info message

  attributeChangedCallback(attr, oldValue, newValue) {
    switch (attr) {
      case 'data-min': this.#min = newValue; break
      case 'data-max': this.#max = newValue; break
      case 'data-integer': this.#integer = this.hasAttribute('data-integer'); break
      case 'disabled': this.#input.disabled = this.hasAttribute('disabled'); break;
    }
  }

  connectedCallback() {
    if (this.disconnected) return
    // input
    this.#input = document.createElement('input')
    this.#input.id = `mint-form-${this.id}`
    this.#input.classList.add('form-border')
    this.#input.style.height = 'var(--form-height)'
    this.#input.oninput = (e) => this.checkData(e)
    this.#input.style.minWidth = '1rem'
    this.#input.disabled = this.hasAttribute('disabled')
    // info message
    this.#info = document.createElement('div')
    this.#info.classList.add('invalid-label')
    // root
    this.#min = this.dataset.min
    this.#max = this.dataset.max
    this.#integer = this.dataset.integer !== undefined
    this.append(this.#input, this.#info)
    this.classList.add('d-grid')
    translateChildren(this)
    watchLocale(() => this.checkData())
  }

  /**
   * Set the input value (without check) and update view
   * @param {Number} newValue 
   */
  setInputValue(newValue) {
    if (comma) this.#input.value = String(newValue).replaceAll('.', ',')
    else this.#input.value = String(newValue).replaceAll(',', '.')
    this.#info.setAttribute('data-hide', '')
    this.#input.classList.remove('invalid')
  }

  /** 
   * Check value of input field
   */
  checkData(event = null) {
    if (event) event.stopPropagation()
    let infoText = ''
    let infoParams = {}
    const val = Number(this.#input.value.replaceAll(',', '.'))
    if (!this.#integer && !this.#input.value.match(regexNumber)) {
      infoText = 'InputNumber.enter-number'
    } else if (this.#integer && !this.#input.value.match(regexInteger)) {
      infoText = 'InputNumber.enter-number'
    } else if (val < Number(this.#min)) {
      infoText = 'InputNumber.min-number'
      infoParams = { min: this.#min + (this.unit ? ' ' + this.unit : '') }
    } else if (val > Number(this.#max)) {
      infoText = 'InputNumber.max-number'
      infoParams = { max: this.#max + (this.unit ? ' ' + this.unit : '') }
    } else if (this.#integer && val - Math.round(val) !== 0) {
      infoText = 'InputNumber.integer-number'
    } else {
      this.#value = val
      if (event?.type === 'input') {
        this.dispatchEvent(new Event('change'))
        this.dispatchEvent(new Event('input'))
      }
      if (comma) this.#input.value = this.#input.value.replaceAll('.', ',')
      else this.#input.value = this.#input.value.replaceAll(',', '.')
    }
    if (infoText) {
      this.#info.setAttribute('data-i18n', infoText)
      this.#info.setAttribute('data-max', infoParams.max)
      this.#info.setAttribute('data-min', infoParams.min)
      this.#info.removeAttribute('data-hide')
      this.#input.classList.add('invalid')
    } else {
      this.#input.classList.remove('invalid')
      this.#info.setAttribute('data-hide', '')
    }
    translateChildren(this)
  }

  /**
   * Focus element
   */
  focus() {
    if (this.#input) this.#input.focus()
  }

  /**
   * Select element
   */
  select() {
    if (this.#input) this.#input.select()
  }

  /**
   * Get value
   */
  get value() {
    return this.#value
  }

  /**
   * Get text value of input field (not numeric value!)
   */
  get text() {
    return this.#input.value
  }

  /**
   * Set value
   */
  set value(newValue) {
    this.#value = newValue
    this.setInputValue(newValue)
  }
}

customElements.define('simple-input-number', SimpleInputNumber)