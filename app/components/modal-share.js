import { translateChildren } from '../modules/i18n.js'
import { ModalDialog } from './modal-dialog.js'
import { importCss } from '../modules/utils.js'
import './qr-code.js'
import './alert-box.js'
importCss('form-components.css')

/**
 * Display a modal dialog containing a link, a QR code, and a button to copy the link.
 * @property {String} [data-text=''] extra text info
 * @property {String} data-url - url to share
 * @see ModalDialog
 */
class ModalShare extends ModalDialog {
  area = null // reference to textarea
  link = null // reference to hyperlink
  copyInfo = null // reference to copy info
  qrCode = null // reference to qrCode

  static observedAttributes = ['data-url']

  connectedCallback() {
    if (this.disconnected) return
    super.connectedCallback()
    // set title
    this.title.setAttribute('data-i18n', 'General.share')
    // extra text
    if (this.dataset.text) {
      const p = document.createElement('p')
      p.setAttribute('data-i18n', this.dataset.text)
      this.body.append(p)
    }
    // text area with url
    this.area = document.createElement('textarea')
    this.area.role = 'link'
    this.area.classList.add('form-border', 'w-100')
    this.area.style.boxSizing = 'border-box'
    this.area.readOnly = true
    this.area.id = Math.random()
    this.body.append(this.area)
    // qr code
    this.qrCode = document.createElement('qr-code')
    this.link = document.createElement('a')
    this.link.target = '_blank'
    this.link.append(this.qrCode)
    const div = document.createElement('div')
    div.classList.add('text-center')
    div.append(this.link)
    this.body.append(div)
    // copy info
    this.copyInfo = document.createElement('alert-box')
    this.copyInfo.setAttribute('data-hide', '')
    this.copyInfo.setAttribute('data-text', 'General.info-copy')
    this.body.append(this.copyInfo)
    // footer
    const buttonCopy = document.createElement('button')
    buttonCopy.classList.add('btn', 'btn-primary')
    buttonCopy.setAttribute('data-i18n', 'General.btn-copy')
    buttonCopy.onclick = () => this.copy()
    const buttonClose = document.createElement('button')
    buttonClose.classList.add('btn', 'btn-secondary')
    buttonClose.setAttribute('data-i18n', 'General.btn-close')
    buttonClose.onclick = () => this.emitClose()
    this.footer.append(buttonCopy, buttonClose)
    this.footer.removeAttribute('data-hide')
    // translate
    translateChildren(this)
  }
     
  attributeChangedCallback(attr, oldValue, newValue) {
    if (attr === 'data-url' && this.link) {
      this.area.value = newValue
      this.link.href = newValue
      this.qrCode.setAttribute('data-text', newValue)
    }
  }

  /**
   * Copy contents of text-area to clipboard
   */
  copy () {
    this.area.select()
    navigator.clipboard.writeText(this.area.value)
    this.copyInfo.showIf()
  }

  /**
   * Close dialog, inherited from ModalDialog
   */
  close () {
    this.copyInfo.hide()
    super.close()
  }
}

customElements.define('modal-share', ModalShare)

