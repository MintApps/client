import { initMath, renderMath } from '../modules/asciimathml.js'
import { translateChildren, useDecimalSeparatorComma } from '../modules/i18n.js'
import { HTMLMintElement } from './abstract-components.js'
import { watch as watchLocale } from '../modules/i18n.js'
import { renderBlock, renderInline } from '../modules/markdown.js'
import { validateText, validateTextArea } from '../modules/validator.js'
import { importCss, ref } from '../modules/utils.js'
import { toSuperScript } from '../modules/text.js'
importCss('form-components.css')

const regexNumber = /^[+-]?[0-9]+(?:[.,][0-9]+)?(?:[eE][+-]?[0-9]+)?$/ // regex for validating number inputs
const regexInteger = /^[+-]?[0-9]+$/ // regex for validating integer inputs

initMath({})
// Watch the current language to determine whether a comma or a period should be used as a decimal separator
let comma = true
watchLocale(() => {
  comma = useDecimalSeparatorComma()
})

// Keep track of element, which has the focus (at the moment only custom select)
const focusElement = new ref(null)

// Calculate mint-id, used for the subelements <label> and <input>
const mintId = (id) => `mint-form-${id}`

/**
 * Abstract base class for all form components
 */
export class AbstractFormElement extends HTMLMintElement {
  static observedAttributes = ['data-hide']
  #disconnected = false
  #label = null // corresponding label

  attributeChangedCallback(attr) {
    // also hide corresponding label
    if (attr === 'data-hide' && this.id) {
      const l = this.label
      if (l) {
        if (this.hasAttribute('data-hide')) l.setAttribute('data-hide', '')
        else l.removeAttribute('data-hide')
      }
    }
  }

  /**
   * Show element (and corresponding mint-label)
   */
  show() {
    const l = this.label
    if (l) l.show()
    super.show()
  }

  /**
   * Hide element (and corresponding mint-label)
   */
  hide() {
    const l = this.label
    if (l) l.hide()
    super.hide()
  }

  /**
   * Show or hide element (and corresponding mint-label)
   * @param {Boolean} state - condition
   */
  showIf(condition) {
    const l = this.label
    if (l) l.showIf(condition)
    super.showIf(condition)
  }

  /**
   * Get corresponding mint-label element
   * @retursn {MintLabel}
   */
  get label() {
    if (!this.#label) {
      this.#label = this.parentElement.querySelector(`[for=${this.id}]`)
      if (!this.#label) console.error(`Label for id ${this.id} missing`)
    }
    return this.#label
  }

  connectedCallback() {
    this.#disconnected = false
  }

  disconnectedCallback() {
    this.#disconnected = true
  }

  get disconnected() {
    return this.#disconnected
  }
}

/**
 * Creates the HTML elments of a label taking into account the text in 'data-text', the formula symbol in 'data-math' and the unit in 'data-unit'
 * @property {String} [data-text] - text part of label
 * @property {String} [data-math] - formula symbol of label
 * @property {String} [data-unit] - unit of label
 */
export class MintLabel extends HTMLMintElement {
  static observedAttributes = ['data-text', 'data-unit', 'data-math']
  #noData = ['math', 'text', 'unit', 'hide'] // data attributes which are no parameters for i18n

  connectedCallback() {
    this.renderLabel()
    translateChildren(this)
  }

  /**
   * Render text, math and unit of label
   */
  renderLabel() {
    this.innerHTML = ''
    const label = document.createElement('label')
    label.setAttribute('for', mintId(this.getAttribute('for')))
    // text part of label
    if (this.dataset.text) {
      const element = document.createElement('span')
      element.setAttribute('data-i18n-md-inline', this.dataset.text)
      Object.keys(this.dataset).forEach(key => {
        if (this.#noData.indexOf(key) <= 0) element.setAttribute(`data-${key}`, this.dataset[key])
      })
      element.style.hyphens = 'auto'
      if (this.dataset.math) element.style.marginRight = '0.4rem'
      label.append(element)
    }
    // only math part in label
    if (this.dataset.math && !this.dataset.unit) {
      const element = document.createElement('span')
      element.innerHTML = renderMath(this.dataset.math)
      label.append(element)
    }
    // only unit part in label
    else if (!this.dataset.math && this.dataset.unit) {
      const element = document.createElement('span')
      element.innerHTML = '&nbsp;' + renderMath(`// unit(${this.dataset.unit}`)
      label.append(element)
    }
    // math & unit part in label
    else if (this.dataset.math && this.dataset.unit) {
      const element = document.createElement('span')
      element.innerHTML = renderMath(`${this.dataset.math} // unit(${this.dataset.unit})`)
      label.append(element)
    }
    this.append(label)
    // style
    this.classList.add('mint-label')
  }

  attributeChangedCallback() {
    this.renderLabel()
    translateChildren(this)
  }
}

/**
 * @property {Boolean} [data-slider] show optional slider (not reactive)
 * @property {Boolean} [data-integer] require integer numbers (not reactive)
 * @property {Boolean} [disabled] disable input
 * @property {Number} [data-step=0.1]
 * @property {Number} [data-min=0]
 * @property {Number} [data-max=1]
 */
class InputNumber extends AbstractFormElement {
  static observedAttributes = ['disabled', 'data-step', 'data-min', 'data-max', 'data-integer', 'data-hide']

  #step = 1 // Step size for changing the value
  #min = 0 // Minimum value
  #max = 10 // Maximum value
  #timer = null // Internal timer for longer touch or mouse events
  #timerCounter = 0 // Internal counter for timer, used to increase the speed
  #integer = false // Accept only interger values
  #value = undefined // Current value of input field
  #input = null // Reference to input element
  #sliderInput = null // Reference to slider input element
  #leftBtn = null // Reference to left - button
  #rightBtn = null // Reference to right + button
  #info = null // Reference to optional info message

  attributeChangedCallback(attr, oldValue, newValue) {
    super. attributeChangedCallback(attr, oldValue, newValue)
    switch (attr) {
      case 'data-min': this.#min = newValue; this.checkData(); break
      case 'data-max': this.#max = newValue; this.checkData(); break
      case 'data-step': this.#step = newValue; break
      case 'data-integer': this.#integer = this.hasAttribute('data-integer'); break
      case 'disabled': this.updateDisabledState(); break
    }
    if (this.#sliderInput) {
      this.#sliderInput.min = this.#min
      this.#sliderInput.max = this.#max
      this.#sliderInput.step = this.#step
    }
    this.updateDisabledState()
  }

  connectedCallback() {
    if (this.disconnected) return
    // input
    this.#input = document.createElement('input')
    this.#input.id = `mint-form-${this.id}`
    this.#input.classList.add('flex-grow', 'form-border', 'border-right-none', 'border-left-none', 'math-font')
    this.#input.oninput = (e) => this.checkData(e)
    this.#input.onfocus = () => focusElement.value = this
    // left-button
    this.#leftBtn = document.createElement('button')
    this.#leftBtn.classList.add('btn', 'btn-input')
    this.#leftBtn.onmousedown = (e) => this.startTimer(e, -1)
    this.#leftBtn.onmouseup = (e) => this.stopTimer(e)
    this.#leftBtn.onmouseleave = (e) => this.stopTimer(e)
    this.#leftBtn.ontouchstart = (e) => this.startTimer(e, -1)
    this.#leftBtn.ontouchend = (e) => this.stopTimer(e)
    this.#leftBtn.oncontextmenu = (e) => { e.preventDefault() }
    this.#leftBtn.onclick = (e) => { focusElement.value = this; e.preventDefault() }
    this.#leftBtn.setAttribute('data-i18n-title', 'General.decrease')
    const leftImg = document.createElement('img')
    leftImg.src = `../assets/icons/decrease.svg`
    this.#leftBtn.append(leftImg)
    // right button
    this.#rightBtn = document.createElement('button')
    this.#rightBtn.onmousedown = (e) => this.startTimer(e, +1)
    this.#rightBtn.onmouseup = (e) => this.stopTimer(e)
    this.#rightBtn.onmouseleave = (e) => this.stopTimer(e)
    this.#rightBtn.ontouchstart = (e) => this.startTimer(e, +1)
    this.#rightBtn.ontouchend = (e) => this.stopTimer(e)
    this.#rightBtn.oncontextmenu = (e) => { e.preventDefault() }
    this.#rightBtn.onclick = (e) => { focusElement.value = this; e.preventDefault() }
    this.#rightBtn.classList.add('btn', 'btn-input')
    this.#rightBtn.setAttribute('data-i18n-alt', 'General.increase')
    const rightImg = document.createElement('img')
    rightImg.setAttribute('data-i18n-title', 'General.increase')
    rightImg.src = `../assets/icons/increase.svg`
    this.#rightBtn.append(rightImg)
    // inner input container
    const innerInputContainer = document.createElement('div')
    innerInputContainer.classList.add('d-flex', 'form-height')
    innerInputContainer.append(this.#leftBtn, this.#input, this.#rightBtn)
    // info message
    this.#info = document.createElement('div')
    this.#info.classList.add('invalid-label')
    this.append(innerInputContainer, this.#info)
    // optional slider
    if (this.hasAttribute('data-slider')) {
      const slider = document.createElement('div')
      slider.classList.add('custom-slider', 'hide-print')
      this.#sliderInput = document.createElement('input')
      this.#sliderInput.type = 'range'
      this.#sliderInput.min = this.#min
      this.#sliderInput.max = this.#max
      this.#sliderInput.step = this.#step
      this.#sliderInput.oninput = (e) => this.handleSliderInput(e)
      this.#sliderInput.onchange = (e) => this.handleSliderInput(e)
      this.#sliderInput.onfocus = () => focusElement.value = this
      slider.append(this.#sliderInput)
      this.append(slider)
    }
    // root
    this.classList.add('wide-element')
    translateChildren(this)
    if (this.#value !== undefined) this.setInputValue(this.#value)
    this.updateDisabledState()
    watchLocale(() => this.checkData())
  }

  /**
   * Disable or Enable input elements accordingt to attribute 'disabled' 
   */
  updateDisabledState() {
    const disabled = this.hasAttribute('disabled')
    if (this.#input) this.#input.disabled = disabled
    if (this.#sliderInput) this.#sliderInput.disabled = disabled
    if (this.#leftBtn) this.#leftBtn.disabled = disabled || Number(this.#value) <= Number(this.#min)
    if (this.#rightBtn) this.#rightBtn.disabled = disabled || Number(this.#value) >= Number(this.#max)
  }

  /**
   * React to touchstart or mousedown event, change number and start internal timer
   * @param {Event} e - mouse- or touchevent
   * @param {Number} direction - either +1 or -1 (increase or decrease)
   */
  startTimer(e, direction) {
    e.stopPropagation()
    e.preventDefault()
    this.stopTimer()
    this.changeInput(this.#step * direction)
    this.#timerCounter = 0
    this.#timer = window.setInterval(() => {
      this.#timerCounter++
      if (
        (this.#timerCounter < 30 && this.#timerCounter % 3 === 0) ||
        (this.#timerCounter >= 30 && this.#timerCounter < 60 && this.#timerCounter % 2 === 0) ||
        this.#timerCounter >= 60
      ) {
        this.changeInput(this.#step * direction)
      }
    }, 100)
  }

  /**
   * Stop internal time, used on touchend and mouseup events
   */
  stopTimer() {
    if (this.#timer) window.clearInterval(this.#timer)
  }

  /**
   * Change the currentNumber by delta, taking into account the limits (min, max) and the step size
   * @param {Number} delta 
   */
  changeInput(delta) {
    let newInput
    // increase - decrease
    if (this.#step > 0) {
      newInput = Number((Math.floor(this.#value / this.#step + 1e-6) * this.#step + delta).toPrecision(10))
    } else {
      newInput = Number((Math.ceil(this.#value / this.#step - 1e-6) * this.#step + delta).toPrecision(10))
    }
    // ensure range
    newInput = Math.max(Math.min(newInput, this.#max), this.#min)
    // integer ?
    if (this.#integer) newInput = Math.round(newInput)
    // set
    this.#info.innerHTML = ''
    if (newInput === this.#value) return
    this.setInputValue(newInput)
    this.#value = Number(newInput)
    this.dispatchEvent(new Event('change'))
    this.dispatchEvent(new Event('input'))
    this.updateDisabledState()
  }

  /**
   * Set the input value (without check) and update view
   * @param {Number} newValue 
   */
  setInputValue(newValue) {
    if (comma) this.#input.value = String(newValue).replaceAll('.', ',')
    else this.#input.value = String(newValue).replaceAll(',', '.')
    if (this.#sliderInput) this.#sliderInput.value = newValue
    this.#info.setAttribute('data-hide', '')
    this.#input.classList.remove('invalid')
    this.updateDisabledState()
  }

  /** 
   * Check value of input field
   */
  checkData(event = null) {
    if (event) event.stopPropagation()
    if (!this.#input) return
    let infoText = ''
    let infoParams = {}
    const val = Number(this.#input.value.replaceAll(',', '.'))
    if (!this.#integer && !this.#input.value.match(regexNumber)) {
      infoText = 'InputNumber.enter-number'
    } else if (this.#integer && !this.#input.value.match(regexInteger)) {
      infoText = 'InputNumber.enter-number'
    } else if (val < Number(this.#min)) {
      infoText = 'InputNumber.min-number'
      infoParams = { min: this.#min + (this.unit ? ' ' + this.unit : '') }
    } else if (val > Number(this.#max)) {
      infoText = 'InputNumber.max-number'
      infoParams = { max: this.#max + (this.unit ? ' ' + this.unit : '') }
    } else if (this.#integer && val - Math.round(val) !== 0) {
      infoText = 'InputNumber.integer-number'
    } else {
      this.#value = val
      if (this.#sliderInput) this.#sliderInput.value = val
      if (event?.type === 'input') {
        this.dispatchEvent(new Event('change'))
        this.dispatchEvent(new Event('input'))
      }
      if (comma) this.#input.value = this.#input.value.replaceAll('.', ',')
      else this.#input.value = this.#input.value.replaceAll(',', '.')
    }
    if (infoText) {
      this.#info.setAttribute('data-i18n', infoText)
      this.#info.setAttribute('data-max', infoParams.max)
      this.#info.setAttribute('data-min', infoParams.min)
      this.#info.removeAttribute('data-hide')
      this.#input.classList.add('invalid')
    } else {
      this.#input.classList.remove('invalid')
      this.#info.setAttribute('data-hide', '')
    }
    this.updateDisabledState()
    translateChildren(this)
  }

  /**
   * Handle input event from slider
   */
  handleSliderInput(e) {
    e.stopPropagation()
    this.#value = Number(this.#sliderInput.value)
    this.setInputValue(this.#value)
    this.dispatchEvent(new Event('input'))
  }

  /**
   * Get value
   */
  get value() {
    return this.#value
  }

  /**
   * Set value
   */
  set value(newValue) {
    this.#value = newValue
    if (this.#input) this.setInputValue(newValue)
  }
}

/**
 * Custom switch for forms in the Mintapps, corresponds to the HTML input type "checkbox"
 */
class InputSwitch extends AbstractFormElement {
  /** Flag indicating whether the switch is on */
  #checked = false
  /** Reference to img */
  #img = null

  connectedCallback() {
    if (this.disconnected) return
    // button
    this.#img = document.createElement('img')
    this.#img.classList.add('invert-dark')
    this.#img.setAttribute('data-i18n-alt', 'General.switch')
    const button = document.createElement('button')
    button.id = mintId(this.id)
    button.classList.add('btn', 'btn-input')
    button.onclick = (e) => this.changed(e)
    button.append(this.#img)
    // root
    this.updateImg()
    this.append(button)
    translateChildren(this)
  }

  /** Handle click on checkbox */
  changed(e) {
    e.preventDefault()
    focusElement.value = this
    this.#checked = !this.#checked
    this.updateImg()
    this.dispatchEvent(new Event('change'))
    this.dispatchEvent(new Event('input'))
  }

  /** Update image of checkbox according to state */
  updateImg() {
    if (!this.#img) return
    this.#img.src = this.#checked ? `../assets/icons/check.svg` : `../assets/icons/empty.svg`
  }

  /**
   * Return value
   */
  get value() {
    return this.#checked
  }

  /**
   * Set value
   * @param {Boolean} new value
   */
  set value(newValue) {
    this.#checked = newValue
    this.updateImg()
  }

}

/**
 * Custom select for forms in the Mintapps, corresponds to the HTML element "select"
 * @property {String} data-options - definition of available options, for example 'General.none, FranckHertz.setup-1, FranckHertz.setup-2'; the individual options are separated by commas and translated automatically
 * @property {Boolean} [disabled] disable input
 */
class InputSelect extends AbstractFormElement {
  #c = null // reference to collapsed entry
  #e = null // reference to expanded entry
  #expanded = false // expanded flag
  #options = [''] // array of options
  #value = 0 // current selected value
  #disabled = false // element is disabled

  static observedAttributes = ['disabled', 'data-options', 'data-hide']

  constructor() {
    super()
    // watch other expanded elements and close if necessary
    focusElement.watch((oldValue, newValue) => {
      if (newValue !== this) {
        this.#expanded = false
        this.renderCollapsed()
        this.renderExpanded()
      }
    })
  }

  connectedCallback() {
    if (this.disconnected) return
    this.#disabled = this.hasAttribute('disabled')
    this.#value = this.dataset.init ?? 0
    // collapsed and expanded entry
    this.#c = document.createElement('div')
    this.#e = document.createElement('div')
    this.#e.classList.add('d-none')
    this.renderCollapsed()
    this.renderExpanded()
    // container
    this.classList.add('custom-select-container', 'wide-element')
    this.append(this.#c, this.#e)
  }


  attributeChangedCallback(attr, oldValue, newValue) {
    super.attributeChangedCallback(attr, oldValue, newValue)
    if (attr === 'data-options') {
      this.#options = newValue.split(',').map(x => x.trim())
      if (this.#value >= this.#options.length) this.#value = this.#options.length - 1
      this.renderCollapsed()
      this.renderExpanded()
    } else if (attr === 'disabled') {
      this.#disabled = this.hasAttribute('disabled')
      if (this.#disabled) this.#expanded = false
      this.renderCollapsed()
    }
  }

  renderCollapsed() {
    if (!this.#c) return
    this.#c.classList.add('d-flex')
    const div = document.createElement('div')
    // text
    div.classList.add('form-border', 'custom-select')
    div.setAttribute('data-i18n-md-inline', this.#options[this.#value])
    // button
    const img = document.createElement('img')
    img.src = this.#expanded ? `../assets/icons/collapse.svg` : `../assets/icons/expand.svg`
    const button = document.createElement('button')
    button.id = mintId(this.id)
    button.setAttribute('data-i18n-title', 'General.select')
    button.classList.add('btn', 'btn-input')
    button.disabled = this.#disabled
    button.append(img)
    // root
    this.#c.innerHTML = ''
    this.#c.append(div, button)
    if (this.#disabled) div.classList.add('disabled')
    else this.#c.onclick = (e) => this.toggleExpand(e)
    translateChildren(this.#c)
  }

  renderExpanded() {
    // root
    if (!this.#e) return
    this.#e.innerHTML = ''
    if (!this.#expanded) {
      this.#e.classList.add('d-none')
    } else {
      this.#e.classList.remove('d-none')
      this.#e.classList.add('custom-option-container', 'shadow')
      this.#options.forEach((option, nr) => {
        const button = document.createElement('button')
        button.classList.add('custom-option')
        button.value = nr
        button.setAttribute('data-i18n-md-inline', option)
        button.onclick = () => this.handleInput(nr)
        this.#e.append(button)
      })
      translateChildren(this.#e)
    }
  }

  toggleExpand(e) {
    e.preventDefault()
    if (this.#disabled) return
    this.#expanded = !this.#expanded
    if (this.#expanded) focusElement.value = this
    this.renderCollapsed()
    this.renderExpanded()
  }

  handleInput(nr) {
    this.#value = nr
    this.dispatchEvent(new Event('change'))
    this.dispatchEvent(new Event('input'))
    this.#expanded = false
    this.renderCollapsed()
    this.renderExpanded()
  }

  get value() {
    return Number(this.#value)
  }

  set value(x) {
    this.#value = x
    this.#expanded = false
    this.renderCollapsed()
  }
}

/**
 * Custom text input for forms in the Mintapps, corresponds to HTML input / textarea
 * @property {Boolean} [data-multi-line=false] - default to single line input
 * @property {Boolean} [data-password=false]- hide user input
 * @property {Boolean} [data-autocomplete=false] - autocomplete input
 * @property {Boolean} [disabled=false] - disable field
 * @property {Boolean} [data-markdown=false] - enable markdown preview
 * @property {Number} [data-min=0] - minimum number of characters
 * @property {Number} [data-max=1000] - maximum number of characters
 */
class InputText extends AbstractFormElement {
  #multiLine = false // multi line input
  #input = null // input element
  #info = null // error text
  #preview = null // preview element
  #previewButton = null // button to toggle preview
  #showPreview // show preview

  static observedAttributes = ['disabled', 'data-hide']

  connectedCallback() {
    if (this.disconnected) return
    this.#multiLine = this.hasAttribute('data-multi-line')
    // input field
    this.#input = document.createElement(this.#multiLine ? 'textarea' : 'input')
    this.#input.id = mintId(this.id)
    this.#input.classList.add('form-border', 'flex-grow')
    if (this.hasAttribute('data-markdown')) this.#input.classList.add('border-right-none')
    if (this.autocomplete) this.#input.autocomplete = this.autocomplete
    if (!this.#multiLine) this.#input.classList.add('form-height')
    if (this.getAttribute('type') === 'password') this.#input.type = 'password'
    this.#input.autocomplete = this.hasAttribute('data-autocomplete')
    this.#input.oninput = (e) => this.handleInput(e)
    this.#input.onkeydown = (e) => this.handleKeyDown(e)
    this.#input.onfocus = () => focusElement.value = this
    // markdown preview button
    this.#previewButton = document.createElement('button')
    this.#previewButton.classList.add('btn', 'btn-input')
    if (!this.hasAttribute('data-markdown')) this.#previewButton.classList.add('d-none')
    else this.#previewButton.disabled = true
    const img = document.createElement('img')
    img.src = `../assets/icons/preview.svg`
    img.setAttribute('data-i18n-alt', 'General.preview')
    img.setAttribute('data-i18n-title', 'General.preview')
    this.#previewButton.append(img)
    this.#previewButton.onclick = (e) => this.toggleMarkdownPreview(e)
    // input container
    const inputContainer = document.createElement('div')
    inputContainer.classList.add('d-flex')
    if (!this.#multiLine) inputContainer.classList.add('form-height')
    inputContainer.append(this.#input, this.#previewButton)
    // preview
    this.#preview = document.createElement('div')
    this.#preview.classList.add('d-none', 'markdown-preview')
    // error info
    this.#info = document.createElement('div')
    this.#info.classList.add('invalid-label', 'd-none')
    // root
    this.classList.add('wide-element')
    this.append(inputContainer, this.#preview, this.#info)
    this.handleInput()
    translateChildren(this)
  }

   
  attributeChangedCallback(attr, oldValue, newValue) {
    super. attributeChangedCallback(attr, oldValue, newValue)
    if (attr === 'disabled') {
      const disabled = this.hasAttribute('disabled')
      this.querySelectorAll('button').forEach(e => e.disabled = disabled)
      this.#input.disabled = disabled
    }
  }

  /**
   * Handle input event
   * @param {Event} e 
   */
  handleInput(e) {
    if (e) e.stopPropagation()
    let i = this.#input.value
    const min = this.dataset.min ? Number(this.dataset.min) : 0
    const max = this.dataset.max ? Number(this.dataset.max) : 1000
    // validate input
    if (i === undefined || i === null) i = ''
    let info = ''
    let infoParams = {}
    if (i.length < min) {
      info = 'InputText.min-length'
      infoParams = { min }
    } else if (i.length > max) {
      info = 'InputText.max-length'
      infoParams = { max, length: i.length }
    } else if (this.hasAttribute('data-multi-line') && !validateTextArea(i, false, 0)) {
      info = 'InputText.invalid-input'
    } else if (!this.hasAttribute('data-multi-line') && !validateText(i, false, 0)) {
      info = 'InputText.invalid-input'
    }
    // update view
    if (!info) {
      this.#info.classList.add('d-none')
      this.#input.classList.remove('invalid')
      this.#previewButton.disabled = i.length === 0
      if (this.#showPreview) {
        if (i.length === 0) this.toggleMarkdownPreview()
        this.#preview.innerHTML = this.#multiLine ? renderBlock(i) : renderInline(i)
      }
      this.dispatchEvent(new Event('input'))
      this.dispatchEvent(new Event('change'))
    } else {
      this.#previewButton.disabled = true
      this.#info.classList.remove('d-none')
      this.#input.classList.add('invalid')
      this.#info.setAttribute('data-i18n-md-inline', info)
      Object.keys(infoParams).forEach(key => this.#info.setAttribute(`data-${key}`, infoParams[key]))
      translateChildren(this)
    }
  }

  /**
   * Handle key down events to prevent auto submission of forms
   */
  handleKeyDown(e) {
    if (e.keyCode === 13 && !this.#multiLine) {
      e.preventDefault()
      this.dispatchEvent(new Event('enter'))
    }
  }

  /**
   * Toggle markdown preview
   * @param {Event} e 
   */
  toggleMarkdownPreview(e) {
    if (e) e.preventDefault()
    this.#showPreview = !this.#showPreview
    if (this.#showPreview) {
      this.#preview.classList.remove('d-none')
      this.handleInput(e)
    } else {
      this.#preview.classList.add('d-none')
    }
  }

  /**
   * Focus
   */
  focus() {
    if (this.#input) this.#input.focus()
  }

  /** 
   * Set value
   */
  set value(data) {
    this.#input.value = String(data)
    this.handleInput()
  }

  /**
   * Get value
   */
  get value() {
    return this.#input.value
  }
}

/**
 * Input slider for numeric values
 * @property {Number} [data-min=0] - Minimum value
 * @property {Number} [data-max=10] - Maximum value
 * @property {Number} [data-step=1] - Maximum value
 */
class InputSlider extends AbstractFormElement {
  static observedAttributes = ['data-min', 'data-step', 'data-max']
  #input // reference to input element
  #number // reference to number element
  #min // minimum value
  #max // maximum value 
  #step // step
  #value = 0// current value

  connectedCallback() {
    if (this.disconnected) return
    this.#min = this.dataset.min ?? 0
    this.#max = this.dataset.max ?? 10
    this.#step = this.dataset.step ?? 1
    // slider
    const slider = document.createElement('div')
    slider.classList.add('custom-slider')
    this.#input = document.createElement('input')
    this.#input.id =
      this.#input.type = 'range'
    this.#input.min = this.#min
    this.#input.max = this.#max
    this.#input.step = this.#step
    this.#input.tabIndex = 0
    this.#input.oninput = (e) => this.handleInput(e)
    this.#input.onchange = (e) => this.handleInput(e)
    this.#input.onfocus = () => focusElement.value = this
    slider.append(this.#input)
    // number
    this.#number = document.createElement('div')
    this.#number.classList.add('number')
    // root
    this.classList.add('d-flex', 'custom-slider-container', 'form-height', 'wide-element')
    this.append(slider, this.#number)
    this.value = this.dataset.init ?? 0
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    switch (attr) {
      case 'data-min': this.#min = newValue; break
      case 'data-max': this.#max = newValue; break
      case 'data-step': this.#step = newValue; break
    }
    if (this.#input) {
      this.#input.min = this.#min
      this.#input.max = this.#max
      this.#input.step = this.#step
    }
  }

  /**
   * Get value
   */
  get value() {
    return this.#value
  }

  /**
   * Set value
   */
  set value(newValue) {
    this.#value = newValue
    this.#input.value = newValue
  }

  handleInput(e) {
    e.stopPropagation()
    this.#value = Number(this.#input.value)
    this.dispatchEvent(new Event(e.type))
  }
}

/**
 * Color input element
 */
class InputColor extends AbstractFormElement {
  #input = null // reference to input element

  connectedCallback() {
    if (this.disconnected) return
    const separator = document.createElement('div')
    separator.classList.add('flex-grow')
    this.#input = document.createElement('input')
    this.#input.type = 'color'
    this.#input.classList.add('color-input', 'btn-input')
    // don't intercept input and change events, "let them bubble up"
    this.append(separator, this.#input)
    this.classList.add('d-flex', 'form-height', 'wide-element')
  }

  set value(newValue) {
    this.#input.value = newValue
  }

  get value() {
    return this.#input.value
  }
}

/**
 * Date input element
 */
class InputDate extends AbstractFormElement {
  #input = null // reference to input element

  connectedCallback() {
    if (this.disconnected) return
    this.#input = document.createElement('input')
    this.#input.type = 'date'
    this.#input.oninput = () => this.checkData()
    this.#input.classList.add('flex-grow')
    this.append(this.#input)
    this.classList.add('form-height', 'wide-element', 'd-flex')
  }

  set value(newValue) {
    this.#input.value = newValue
  }

  get value() {
    return this.#input.value
  }
}


/**
 * Display a number
 * @property {Number} [data-precision=0] - number of valid digits (0 unlimited)
 * @property {Number} [data-fixed=0] - number of decimal places ( 0 unlimited)
 * @property {Number} [data-scale=1] - scale displayed values by factor
 */
class OutputNumber extends AbstractFormElement {
  #value = 0// current value
  #out // referenct to output div

  connectedCallback() {
    if (this.disconnected) return
    // number
    this.#out = document.createElement('input')
    this.#out.classList.add('form-border', 'mint-output', 'w-100', 'math-font')
    this.#out.id = `mint-form-${this.id}`
    this.#out.readOnly = true
    this.classList.add('wide-element')
    this.append(this.#out)
    this.displayNumber()
    // root
    watchLocale(() => this.displayNumber())
  }

  /**
   * Get value
   */
  get value() {
    return this.#value
  }

  /**
   * Set value
   */
  set value(newValue) {
    this.#value = Number(newValue)
    this.displayNumber()
  }

  /**
   * Format and display number (using current value)
   */
  displayNumber() {
    const scale = this.dataset.scale ?? 1
    const n = this.#value * scale
    const precision = this.dataset.precision ?? 0
    const fixed = this.dataset.fixed ?? 0
    // determine the value with the desired accuracy
    let text
    if (!n && n !== 0) {
      text = '–'
    } else if (precision > 0) {
      text = n.toPrecision(precision)
    } else if (fixed > 0) {
      text = n.toFixed(fixed)
    } else {
      text = String(n)
    }
    // Adjust the decimal separator according to the language
    if (comma) text = text.replaceAll('.', ',')
    // Replace powers of ten with the correct mathematical notation
    const parts = text.split('e')
    if (parts.length !== 1) {
      text = parts[0] + ' · 10' + toSuperScript(parts[1])
    }
    // Update
    this.#out.value = text
  }
}

/**
 * Display a text
 */
class OutputText extends AbstractFormElement {
  #value = 0// current value
  #text // reference text div

  connectedCallback() {
    if (this.disconnected) return
    // number
    this.classList.add('form-border', 'mint-output', 'wide-element')
    this.#value = this.dataset.init ?? '&nbsp;'
    this.#text = document.createElement('div')
    this.append(this.#text)
    this.displayText()
  }

  /**
   * Get value
   */
  get value() {
    return this.#value
  }

  /**
   * Set value
   */
  set value(newValue) {
    this.#value = newValue !== '' ? newValue : '&nbsp;'
    this.displayText()
  }

  /**
   * Format and display text (using current value)
  */
  displayText() {
    // Update
    this.#text.innerHTML = renderInline(this.#value)
  }
}

// Register element
customElements.define('mint-label', MintLabel)
customElements.define('input-switch', InputSwitch)
customElements.define('input-number', InputNumber)
customElements.define('input-select', InputSelect)
customElements.define('input-text', InputText)
customElements.define('input-slider', InputSlider)
customElements.define('input-color', InputColor)
customElements.define('input-date', InputDate)
customElements.define('output-number', OutputNumber)
customElements.define('output-text', OutputText)