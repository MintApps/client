import { emit } from '../modules/utils.js'
import './input-file.js'
import { MintCard } from './mint-card-components.js'
import './modal-dialog.js'

class LocalLoad extends MintCard {
  #input = null // reference to input
  #modal = null // reference to modal
  #jsonFile = null // selected file

  connectedCallback() {
    // title
    this.setAttribute('data-title', 'General.load-local-file')
    super.connectedCallback()
    // body → form
    const form = document.createElement('form')
    form.classList.add('form-container')
    this.#input = document.createElement('input-file')
    this.#input.style.gridColumn = '1/4'
    this.#input.setAttribute('data-file-type', 'json')
    this.#input.setAttribute('data-max-size', '32')
    this.#input.oninput = () => this.fileSelected()
    form.append(this.#input)
    this.body.append(form)
    // modal dialog
    this.#modal = document.createElement('modal-dialog')
    this.#modal.setAttribute('data-title', 'General.confirmation')
    this.#modal.setAttribute('data-buttons', 'cancel, accept')
    this.#modal.setAttribute('data-type', 'accept')
    const p = document.createElement('p')
    p.setAttribute('data-i18n', 'General.confirm-local-load')
    this.#modal.append(p)
    this.#modal.addEventListener('accept', () => this.loadGame())
    this.body.append(this.#modal)
  }

  fileSelected() {
    this.#jsonFile = this.#input.value
    if (window.mintapps.modified) {
      this.#modal.show()
    } else {
      this.loadGame()
    }
  }
  
  loadGame() {
    emit(this, 'load', this.#jsonFile)
  }  
}

customElements.define('local-load', LocalLoad)
