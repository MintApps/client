import { sInit, sRunning, sPaused } from '../modules/states.js'
import { showIf } from '../modules/utils.js'
import { HTMLMintElement } from './abstract-components.js'

/**
 * Row of buttons to control animations
 * @property {Boolean} [data-step=false] show step button
 */
class StateButtons extends HTMLMintElement {
  #value = sInit // current state
  #btnStart // reference start button
  #btnContinue // reference continue button
  #btnStep // reference step button
  #btnPause // reference pause button
  #btnReset // reference reset button
  #onstart // listener start event
  #oncont // listener cont event
  #onstep // listener step event
  #onpause // listener pause event
  #onreset // listener reset event

  connectedCallback() {
    if (this.disconnected) return
    // start button
    this.#btnStart = document.createElement('button')
    this.#btnStart.classList.add('btn', 'btn-primary')
    this.#btnStart.setAttribute('data-i18n', 'General.btn-start')
    this.#btnStart.onclick = () => this.updateState('start')
    // continue button
    this.#btnContinue = document.createElement('button')
    this.#btnContinue.classList.add('btn', 'btn-primary')
    this.#btnContinue.setAttribute('data-i18n', 'General.btn-continue')
    this.#btnContinue.onclick = () => this.updateState('cont')
    // step button
    this.#btnStep = document.createElement('button')
    this.#btnStep.classList.add('btn')
    this.#btnStep.setAttribute('data-i18n', 'General.btn-step')
    this.#btnStep.onclick = () => this.updateState('step')
    // pause button
    this.#btnPause = document.createElement('button')
    this.#btnPause.classList.add('btn')
    this.#btnPause.setAttribute('data-i18n', 'General.btn-pause')
    this.#btnPause.onclick = () => this.updateState('pause')
    // reset button
    this.#btnReset = document.createElement('button')
    this.#btnReset.classList.add('btn', 'btn-secondary')
    this.#btnReset.setAttribute('data-i18n', 'General.btn-reset')
    this.#btnReset.onclick = () => this.updateState('reset')
    // extra content
    const extra = this.childNodes
    // root
    this.append(this.#btnStart, this.#btnContinue, this.#btnStep, this.#btnPause, ...extra, this.#btnReset)
    this.updateView()
  }

  /**
   * Set state
   */
  set value(s) {
    this.#value = s
    this.updateView()
  }

  /**
   * Get current state
   */
  get value() {
    return this.#value
  }

  /**
   * React to button press and update state
   */
  updateState(button) {
    switch (button) {
      case 'start':
        this.#value = sRunning
        if (this.#onstart) this.#onstart()
        break
      case 'cont':
        this.#value = sRunning
        if (this.#oncont) this.#oncont()
        break
      case 'step':
        this.#value = sPaused
        if (this.#onstep) this.#onstep()
        break
      case 'pause':
        this.#value = sPaused
        if (this.#onpause) this.#onpause()
        break
      case 'reset':
        this.#value = sInit
        if (this.#onreset) this.#onreset()
        break
    }
    this.dispatchEvent(new Event(button))
    this.updateView()
  }

  /**
   * Show / hide the appropriate buttons for the current state
   */
  updateView() {
    showIf(this.#btnStart, this.#value === sInit)
    showIf(this.#btnContinue, this.#value === sPaused)
    showIf(this.#btnStep, this.hasAttribute('data-step') && (this.#value == sPaused || this.#value == sInit))
    showIf(this.#btnPause, this.#value === sRunning)
    showIf(this.#btnReset, this.#value !== sInit)
  }

  /**
   * Switch to pause state when currently running
   */
  pause() {
    if (this.#value === sRunning) this.updateState('pause')
  }

  /**
   * Listener for start event
   * @param {Function} listener 
   */
  set onstart(listener) {
    this.#onstart = listener
  }

  /**
   * Listener for pause event
   * @param {Function} listener 
   */
  set onpause(listener) {
    this.#onpause = listener
  }

  /**
  * Listener for cont event
  * @param {Function} listener 
  */
  set oncont(listener) {
    this.#oncont = listener
  }

  /**
   * Listener for step event
   * @param {Function} listener 
   */
  set onstep(listener) {
    this.#onstep = listener
  }

  /**
  * Listener for reset event
  * @param {Function} listener 
  */
  set onreset(listener) {
    this.#onreset = listener
  }

}

customElements.define('state-buttons', StateButtons)