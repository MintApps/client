export class HTMLMintElement extends HTMLElement {

  #disconnected = false // wether element has been disconnected, avoid rebuild on connectedCallback

  /**
   * Check if element is hidden
   * @returns  {Boolean}
   */
  isHidden() {
    return this.hasAttribute('data-hide')
  }

  /**
   * Check if element is visible
   * @returns  {Boolean}
   */
  isVisible() {
    return !(this.hasAttribute('data-hide'))
  }

  /**
   * Hide element
   */
  hide() {
    this.setAttribute('data-hide', '')
  }

  /**
   * Show element
   */
  show() {
    this.removeAttribute('data-hide')
  }

  /**
   * Show or hide element
   * @param {Boolean} state - condition
   */
  showIf(state = true) {
    if (state) this.removeAttribute('data-hide')
    else this.setAttribute('data-hide', '')
  }

  /**
   * Rember, when element has been disconnected
   */
  disconnectedCallback() {
    this.#disconnected = true
  }


  /**
   * Check if element has been disconnected
   * @requires {Boolean}
   */
  get disconnected() {
    return this.#disconnected
  }

  /**
   * Set disabled state
   */
  set disabled (value) {
    if (value) this.setAttribute('disabled', '')
    else this.removeAttribute('disabled')
  }

  /**
   * Get disabled state
   */
  get disabled() {
    return this.hasAttribute('disabled')
  }
}

