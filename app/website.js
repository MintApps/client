/**
 * Website specific settings, 
 * the script should be included directly in the HTML header (and only there).
 * You should also set `website: true` in the configuration (to display the navbars)
 */

/* set background color according to style in order to avoid flash on page reload */
if (sessionStorage.getItem('theme') === '"dark"') document.documentElement.style.backgroundColor = '#000000'

let link, meta

/* load website specific styles */
link = document.createElement('link')
link.rel = 'stylesheet'
link.href = '../assets/styles/website.css'
document.head.append(link)

/* load mintapp main style */
link = document.createElement('link')
link.rel = 'stylesheet'
link.href = '../assets/styles/main.css'
document.head.append(link)

/* set favicons */
for (let size of [64, 180, 196, 512]) {
  link = document.createElement('link')
  link.setAttribute('rel', 'icon')
  link.setAttribute('type', 'image/png')
  link.setAttribute('sizes', `${size}x${size}`)
  link.href = `/favicon${size}.png`
  document.head.append(link)
}

/* apple touch icons */
link = document.createElement('link')
link.setAttribute('rel', 'apple-touch-icon')
link.href = '/touchicon180.png'
document.head.append(link)

/* set viewport */
meta = document.createElement('meta')
meta.name='viewport'
meta.setAttribute('content', 'width=device-width,initial-scale=1.0')
document.head.append(meta)
