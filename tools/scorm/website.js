/**
 * SCORM specific settings
 * the script should be included directly in the HTML header (and only there).
 * You should set `website: false` in the configuration (to hide the navbars)
 */

/* load scorm specific styles */
let link = document.createElement('link')
link.rel = 'stylesheet'
link.href = '../assets/styles/scorm.css'
document.head.append(link)

/* load mintapp main style */
link = document.createElement('link')
link.rel = 'stylesheet'
link.href = '../assets/styles/main.css'
document.head.append(link)

